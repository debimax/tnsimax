class Noeud:
    '''noeud d’un arbre binaire'''

    def __init__(self, valeur, gauche=None, droit=None):
        self.gauche = gauche
        self.valeur = valeur
        self.droit = droit


class Cellule:
    """une cellule d'une liste chaînée"""

    def __init__(self, v, s):
        self.valeur = v
        self.suivante = s


class File():
    """structure de file"""

    def __init__(self):
        self.tete = None
        self.queue = None

    def est_vide(self):
        return self.tete is None

    def enfile(self, x):
        c = Cellule(x, None)
        if self.est_vide():
            self.tete = c
        else:
            self.queue.suivante = c
        self.queue = c

    def defile(self):
        if self.est_vide():
            raise IndexError("retirer sur une file vide")
        v = self.tete.valeur
        self.tete = self.tete.suivante
        if self.tete is None:
            self.queue = None
        return v


def draw(arbre, node_attribut={}, feuille=True, fichier=None):
    '''
    Fonction  pour afficher le graphe avec graphivz.
    > draw(abr)
    ceci affiche le code pour afficher le graphe dans un notebook.
    > draw(abr,node_attribut={'color':'pink','width':'0.3', 'fontcolor':'black'})
    node_attribut est un dictionnaire pour les attributs du nœud
    Par defaut:   {'color': 'lightblue2', 'style': 'filled', 'shape': 'circle', 'fontname': "times bold", 'fontsize': '10', 'width': '0.3'}
    > draw(abr, feuille=True)
    feuille=True permet de tracer ou non les deux arêtes partant des feuilles
    draw(abr,fichier='image.svg')
    fichier permet d'enregistrer la figure dans un format svg, png, jpg, eps, pdf
    '''

    from graphviz import Digraph
    import pydot
    import copy
    # copy  pour ne pas modifier l'arbre
    arbre2 = copy.copy(arbre)

    # parcours en largeur 1 de l'arbre
    # modification des valeurs sous la forme (compteur,valeur)
    compteur = 0
    file = File()
    file.enfile(arbre2)
    while not file.est_vide():
        y = file.defile()
        compteur += 1
        y.valeur = (compteur, y.valeur)
        if y.gauche is not None:
            file.enfile(y.gauche)
        if y.droit is not None:
            file.enfile(y.droit)

    # parcours en largeur 2 de l'arbre pour la création du graphe
    dot = Digraph()
    # Les valeurs par defaut
    dot.node_attr = { 
        'color': 'lightblue2',
        'style': 'filled',
        'shape': 'circle',
        'fontname': "times bold",
        'fontsize': '10',
        'width': '0.3'}

    dot.node(name=str(arbre2.valeur[0]), label=str(arbre2.valeur[1]))
    file = File()
    file.enfile(arbre2)
    while not file.est_vide():
        y = file.defile()
        # print(y.valeur)

        if y.gauche is not None:
            file.enfile(y.gauche)
            # S'il y a un arbre gauche on construit le nœud de ses fils gauche
            # et droit (s'il est vide) ainsi que les arêtes.
            dot.node(
                name=str(
                    y.gauche.valeur[0]), label=str(
                    y.gauche.valeur[1]))
            dot.edge(str(y.valeur[0]), str(y.gauche.valeur[0]))
            # S'il n'y a pas d'arbre droit  on en ajoute un nœud droit (après
            # le gauche) invisible
            if not y.droit:
                dot.node(name=str(y.valeur[0]) + "FD", label="", style="invis")
                dot.edge(str(y.valeur[0]), str(y.valeur[0]) + "FD")

        if y.droit is not None:
            file.enfile(y.droit)
            # S'il n'y a pas d'arbre gauche  on  ajoute un nœeud gauche (avant
            # le droit) invisible
            if not y.gauche:
                dot.node(name=str(y.valeur[0]) + "FG", label="", style="invis")
                dot.edge(str(y.valeur[0]), str(y.valeur[0]) + "FG")
            # et  on construit le nœud fils droit ainsi que l'arètes.
            dot.node(name=str(y.droit.valeur[0]), label=str(y.droit.valeur[1]))
            dot.edge(str(y.valeur[0]), str(y.droit.valeur[0]))

        # ajout des nœuds et arêtes après les feuilles
        if (not y.gauche) and (not y.droit) and feuille:

            dot.node(
                name=str(
                    y.valeur[0]) + "FG",
                label="",
                style="invis")   # FG  pour fils gauche
            dot.node(
                name=str(
                    y.valeur[0]) + "FD",
                label="",
                style="invis")   # FG  pour fils droit
            dot.edge(str(y.valeur[0]), str(y.valeur[0]) + "FG")
            dot.edge(str(y.valeur[0]), str(y.valeur[0]) + "FD")

    #  Pour convertir dans un autre format
    if fichier is not None:
        from graphviz import render
        import os

        ext = fichier.split('.')[-1]
        namefile = "".join(fichier.split('.')[0:-1])
        if ext in ["svg", "png", "jpg", "eps", "pdf", "ps"]:
            dot.format = ext
            dot.render(namefile, view=False, formatter=None)
        #  On efface le fichier source dot
        os.remove(namefile)

    return dot


#arbre = Noeud("F")
#arbre.gauche = Noeud("B")
#arbre.gauche.gauche = Noeud("A")
#arbre.gauche.droit = Noeud("D")
#arbre.gauche.droit.gauche = Noeud("C")
#arbre.gauche.droit.droit = Noeud("E")
#
#arbre.droit = Noeud("G")
#arbre.droit.droit = Noeud("I")
#arbre.droit.droit.gauche = Noeud("I")


#draw(arbre, fichier="test.pdf")
