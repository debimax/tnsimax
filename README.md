# TNSImax


Mes cours pour la classe de TNSI  2020-2021

## Liens vers mes cours
- [TNSImax sur mybinder](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fdebimax%2Ftnsimax/master)

- Mes anciens cours de 1° [2019-2020](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fdebimax%2Fcours-debimax/master)



## Evaluation en terminale

- Programme: [BO de février 2020](https://www.education.gouv.fr/bo/20/Special2/MENE2001797N.htm).

### Nature de l'épreuve  de NSI au bac en terminale

-  Durée : 3 heures 30 + 1 heure
-  Coefficient : 16
-  Format : L'épreuve terminale obligatoire de spécialité est composée de deux parties :
  - une partie écrite, comptant pour 12 points sur 20,
  - et une partie pratique comptant pour 8 points sur 20.  
La note globale de l'épreuve est donnée sur 20 points.

#### Partie écrite de l'épreuve  de NSI au bac en terminale

- Durée : 3 heures 30
- Modalités
  - La partie écrite consiste en la résolution de trois exercices permettant d'évaluer les connaissances et les capacités attendues conformément aux programmes de première et de terminale de la spécialité.
  - Chaque exercice est noté sur 4 points.
  - Le sujet propose cinq exercices, parmi lesquels le candidat choisit les trois qu'il traitera.
  -  Ces cinq exercices permettent d'aborder les différentes rubriques du programme, sans obligation d'exhaustivité. Le sujet comprend obligatoirement au moins un exercice relatif à chacune des trois rubriques suivantes : traitement de données en tables et bases de données ; architectures matérielles, systèmes d'exploitation et réseaux ; algorithmique, langages et programmation.

 
#### Partie pratique de l'épreuve  de NSI au bac en terminale

- Durée : 1 heure
-  **Modalités**
    - La partie pratique consiste en la résolution de deux exercices sur ordinateur, chacun étant noté sur 4 points.
    - Le candidat est évalué sur la base d'un dialogue avec un professeur-examinateur.
    - Un examinateur évalue au maximum quatre élèves. L'examinateur ne peut pas évaluer un élève qu'il a eu en classe durant l'année en cours.
    - L'évaluation de cette partie se déroule au cours du deuxième trimestre pendant la période de l'épreuve écrite de spécialité.
- **Premier exercice**
  - Le premier exercice consiste à programmer un algorithme figurant explicitement au programme, ne présentant pas de difficulté particulière, dont on fournit une spécification.
  - Il s'agit donc de restituer un algorithme rencontré et travaillé à plusieurs reprises en cours de formation.
  - Le sujet peut proposer un jeu de test avec les réponses attendues pour permettre au candidat de vérifier son travail.
- **Deuxième exercice**
  - Pour le second exercice, un programme est fourni au candidat.
  - Cet exercice ne demande pas l'écriture complète d'un programme, mais permet de valider des compétences de programmation suivant des modalités variées : le candidat doit, par exemple, compléter un programme « à trous » afin de répondre à une spécification donnée, ou encore compléter un programme pour le documenter, ou encore compléter un programme en ajoutant des assertions, etc.

##  Présentation du « Grand oral

L'épreuve du « Grand oral » a été conçue pour permettre au candidat de montrer sa capacité à prendre la parole en public de façon claire et convaincante. Elle lui permettra aussi d'utiliser les connaissances liées à ses spécialités pour démontrer ses capacités argumentatives et la maturité de son projet de poursuite d'études, voire professionnel.

- Épreuve orale
- Durée : 20 minutes
- Préparation : 20 minutes
- Coefficient : 10 [voie générale] et 14 [voie technologique]



Le Grand oral dure 20 minutes avec 20 minutes de préparation.

Le candidat présente au jury deux questions préparées avec ses professeurs et éventuellement avec d'autres élèves, qui portent sur ses deux spécialités, soit prises isolément, soit abordées de manière transversale en voie générale. Pour la voie technologique, ces questions s'appuient sur l'enseignement de spécialité pour lequel le programme prévoit la réalisation d'une étude approfondie.

Le jury choisit une de ces deux questions. Le candidat a ensuite 20 minutes de préparation pour mettre en ordre ses idées et créer s'il le souhaite un support (qui ne sera pas évalué) à donner au jury.

L'épreuve se déroule en 3 temps :
-  Pendant 5 minutes, le candidat présente la question choisie et y répond. Le jury évalue son argumentation et ses qualités de présentation. L'exposé se déroule sans note et debout, sauf aménagements pour les candidats à besoins spécifiques.
-  Ensuite, pendant 10 minutes, le jury échange avec le candidat et évalue la solidité de ses connaissances et ses compétences argumentatives. Ce temps d'échange permet à l'élève de mettre en valeur ses connaissances, liées au programme des spécialités suivies en classe de première et terminale.
-  Les 5 dernières minutes d'échanges avec le jury portent sur le projet d'orientation du candidat. Le candidat montre que la question traitée a participé à la maturation de son projet de poursuite d'études, et même pour son projet professionnel.
-  Le jury va porter son attention sur la solidité des connaissances, la capacité à argumenter et à relier les savoirs, l'expression et la clarté du propos, l'engagement dans la parole, la force de conviction et la manière d'exprimer une réflexion personnelle, ainsi qu'aux motivations du candidat.
Candidats individuels ou issus des établissements privés hors contrat
-  Les candidats individuels ou les candidats issus des établissements scolaires privés hors contrat présentent l'épreuve orale terminale dans les mêmes conditions que les candidats scolaires. Le document précisant les questions présentées par le candidat à destination du jury est alors constitué par le candidat lui-même, en conformité avec le cadre défini pour les candidats scolaires.
Composition du jury

Le jury est composé de deux professeurs de disciplines différentes, dont l'un représente l'un des deux enseignements de spécialité du candidat et l'autre représente l'autre enseignement de spécialité ou l'un des enseignements communs, ou est professeur-documentaliste.


## Date  session  2020-2021

- Épreuves de spécialité:  les lundi 15 et le mardi 16 mars 2021.
- Épreuves pratiques et orales des enseignements de spécialité:  du jeudi 18 au vendredi 26 mars.
- Épreuves du Grand oral: du lundi 21 juin au vendredi 2 juillet.
- Dates des résultats du bac:  Le mardi 6 juillet 2021.
- Dates des épreuves de rattrapages du bac:  Du mercredi 7 au vendredi 9 juillet 2021. 
